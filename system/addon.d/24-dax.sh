#!/sbin/sh

# /system/addon.d/24-dax.sh

. /tmp/backuptool.functions

CONFIG_FILE=/system/etc/audio_effects.conf
VENDOR_CONFIG=/system/vendor/etc/audio_effects.conf
OTHER_VENDOR_FILE=/system/etc/audio_effects_vendor.conf
OFFLOAD_CONFIG=/system/etc/audio_effects_offload.conf

list_files() {
cat <<EOF
addon.d/24-dax.sh
priv-app/Ax.apk
priv-app/AxUI.apk
priv-app/Ax/Ax.apk
priv-app/AxUI/AxUI.apk
lib/soundfx/libswdax.so
lib/soundfx/libhwdax.so
lib64/libhwdaphal.so
lib/libhwdaphal.so
lib/libdlbdapstorage.so
lib/libstagefright_soft_ddpdec.so
etc/dolby/dax-default.xml
EOF
}

case "$1" in
	backup)
		list_files | while read FILE DUMMY; do
		backup_file $S/$FILE
	done
	;;
	restore)+
	list_files | while read FILE REPLACEMENT; do
		R=""
		[ -n "$REPLACEMENT" ] && R="$S/$REPLACEMENT"
		[ -f "$C/$S/$FILE" ] && restore_file $S/$FILE $R
	done
	;;
	pre-backup)
		# Stub
	;;
	post-backup)
		# Stub
	;;
	pre-restore)
		# Stub
	;;
	post-restore)
	sed -i '/dax {/,/}/d' $CONFIG_FILE
	sed -i '/dax_sw {/,/}/d' $CONFIG_FILE
	sed -i '/dax_hw {/,/}/d' $CONFIG_FILEx
	
	if [ -f $VENDOR_CONFIG ];
	then
		sed -i '/dax {/,/}/d' $VENDOR_CONFIG
		sed -i '/dax_sw {/,/}/d' $VENDOR_CONFIG
		sed -i '/dax_hw {/,/}/d' $VENDOR_CONFIG
	fi
	
	if [ -f $OTHER_VENDOR_FILE ];
	then
		sed -i '/dax {/,/}/d' $OTHER_VENDOR_FILE
		sed -i '/dax_sw {/,/}/d' $OTHER_VENDOR_FILE
		sed -i '/dax_hw {/,/}/d' $OTHER_VENDOR_FILE
	fi
	
	if [ -f $OFFLOAD_CONFIG ];
	then
		sed -i '/dax {/,/}/d' $OFFLOAD_CONFIG
		sed -i '/dax_sw {/,/}/d' $OFFLOAD_CONFIG
		sed -i '/dax_hw {/,/}/d' $OFFLOAD_CONFIG
	fi
	
	sed -i 's/^libraries {/libraries {\n  dax_sw {\n    path \/system\/vendor\/lib\/soundfx\/libswdax.so\n  }/g' $CONFIG_FILE
	sed -i 's/^libraries {/libraries {\n  dax_hw {\n    path \/system\/vendor\/lib\/soundfx\/libhwdax.so\n  }/g' $CONFIG_FILE
	sed -i 's/^effects {/effects {\n  dax {\n    library proxy\n    uuid 9d4921da-8225-4f29-aefa-6e6f69726861\n\n    libsw {\n      library dax_sw\n      uuid 6ab06da4-c516-4611-8166-6168726e6f69\n    }\n\n    libhw {\n      library dax_hw\n      uuid a0c30891-8246-4aef-b8ad-696f6e726861\n    }\n  }/g' $CONFIG_FILE
	
	if [ -f $VENDOR_CONFIG ];
	then
		sed -i 's/^libraries {/libraries {\n  dax_sw {\n    path \/system\/vendor\/lib\/soundfx\/libswdax.so\n  }/g' $VENDOR_CONFIG
		sed -i 's/^libraries {/libraries {\n  dax_hw {\n    path \/system\/vendor\/lib\/soundfx\/libhwdax.so\n  }/g' $VENDOR_CONFIG
		sed -i 's/^effects {/effects {\n  dax {\n    library proxy\n    uuid 9d4921da-8225-4f29-aefa-6e6f69726861\n\n    libsw {\n      library dax_sw\n      uuid 6ab06da4-c516-4611-8166-6168726e6f69\n    }\n\n    libhw {\n      library dax_hw\n      uuid a0c30891-8246-4aef-b8ad-696f6e726861\n    }\n  }/g' $VENDOR_CONFIG
	fi
	
	if [ -f $OTHER_VENDOR_FILE ];
	then
		sed -i 's/^libraries {/libraries {\n  dax_sw {\n    path \/system\/vendor\/lib\/soundfx\/libswdax.so\n  }/g' $OTHER_VENDOR_FILE
		sed -i 's/^libraries {/libraries {\n  dax_hw {\n    path \/system\/vendor\/lib\/soundfx\/libhwdax.so\n  }/g' $OTHER_VENDOR_FILE
		sed -i 's/^effects {/effects {\n  dax {\n    library proxy\n    uuid 9d4921da-8225-4f29-aefa-6e6f69726861\n\n    libsw {\n      library dax_sw\n      uuid 6ab06da4-c516-4611-8166-6168726e6f69\n    }\n\n    libhw {\n      library dax_hw\n      uuid a0c30891-8246-4aef-b8ad-696f6e726861\n    }\n  }/g' $OTHER_VENDOR_FILE
	fi
	
	if [ -f $OFFLOAD_CONFIG ];
	then
		sed -i 's/^libraries {/libraries {\n  dax_sw {\n    path \/system\/vendor\/lib\/soundfx\/libswdax.so\n  }/g' $OFFLOAD_CONFIG
		sed -i 's/^libraries {/libraries {\n  dax_hw {\n    path \/system\/vendor\/lib\/soundfx\/libhwdax.so\n  }/g' $OFFLOAD_CONFIG
		sed -i 's/^effects {/effects {\n  dax {\n    library proxy\n    uuid 9d4921da-8225-4f29-aefa-6e6f69726861\n\n    libsw {\n      library dax_sw\n      uuid 6ab06da4-c516-4611-8166-6168726e6f69\n    }\n\n    libhw {\n      library dax_hw\n      uuid a0c30891-8246-4aef-b8ad-696f6e726861\n    }\n  }/g' $OFFLOAD_CONFIG
	fi
	;;
esac
