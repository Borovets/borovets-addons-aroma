#!/sbin/sh
#
#Rom Improver Tools
#created by raulx222 and modified by EatBeans
#

case "$1" in
	save)
	rm -f /sdcard/TWRP/Borovets-Addons/checkbox1.prop
	rm -f /sdcard/TWRP/Borovets-Addons/checkbox2.prop
	[ ! -d /sdcard/TWRP/Borovets-Addons ] && install -d /sdcard/TWRP/Borovets-Addons
	cp /tmp/aroma/checkbox1.prop /sdcard/TWRP/Borovets-Addons/checkbox1.prop
	cp /tmp/aroma/checkbox2.prop /sdcard/TWRP/Borovets-Addons/checkbox2.prop
	;;
	load)
	[ -f /sdcard/TWRP/Borovets-Addons/checkbox1.prop ] && { rm -f /tmp/aroma/checkbox1.prop; cp /sdcard/TWRP/Borovets-Addons/checkbox1.prop /tmp/aroma/checkbox1.prop;}
	[ -f /sdcard/TWRP/Borovets-Addons/checkbox2.prop ] && { rm -f /tmp/aroma/checkbox2.prop; cp /sdcard/TWRP/Borovets-Addons/checkbox2.prop /tmp/aroma/checkbox2.prop;}
	;;
	reset)
	rm -f /tmp/aroma/checkbox1.prop
	rm -f /tmp/aroma/checkbox2.prop
	;;
esac
